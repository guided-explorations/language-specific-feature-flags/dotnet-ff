using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspMVC.Models;
using Unleash;

namespace AspMVC.Controllers
{
    public class HomeController : Controller
    {
        private DefaultUnleash _unleash;

        public HomeController()
        {

            var settings = new UnleashSettings()
            {
                AppName = Environment.GetEnvironmentVariable("ENVIRONMENT_NAME"),
                InstanceTag = Environment.GetEnvironmentVariable("UNLEASH_API_KEY"),
                UnleashApi = new Uri(Environment.GetEnvironmentVariable("UNLEASH_API_URL"))
            };

            _unleash = new DefaultUnleash(settings);
        }

        public IActionResult Index()
        {
            var ivm = new IndexViewModel();
            ivm.Heading = "My Index";
;
            if (_unleash.IsEnabled("test")) {
                ivm.Heading = "New Index unleashed";
            }

            return View(ivm);
            // return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
